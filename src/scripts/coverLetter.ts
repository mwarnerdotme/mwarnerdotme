export default console.log(`Welcome to my website!

Working with web applications and software is what I truly love doing - both as a career as well as a hobby. My other passions usually involve music, reading, fitness, or some kind of day-long outdoor adventure.

Thanks for visiting, and feel free to reach out if you'd like to make a genuine connection!

Looking forward to it,
Michael Warner
`)
